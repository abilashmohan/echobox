var cutils = require("./cutils");
var createTimeRange = (startTime, endTime, interval) => {
    var retVal = [];
    var currTime = startTime;
    do {
        var rechargeSlab = { time: currTime, recharge : cutils.currencyRound(Math.random() * 1000)};
        retVal.push(rechargeSlab);
        currTime += Math.round(Math.random() * interval);
    } while (currTime <= endTime);
    return retVal;
};

module.exports = {
    createTimeRange: createTimeRange
};