"use strict";

var requestify = require("requestify");
var loginURL = "https://eu.lightify-api.org/lightify/services/session";
var baseURL = "https://eu.lightify-api.org/services";
var idx = 1;

var getLoginPayload = () => ({
    username: "allajunaki@gmail.com",
    password: "lightifypoc",
    serialNumber: "OSR017C5371"
});
var intentMap = {
    "Good": "00FF00",
    "Happy": "AEC6CF",
    "Warn": "FFE000",
    "Cheerful": "FFA500",
    "Danger": "FF0000"
};

var constructBasicPattern = (authorization, params) => ({
    method: "GET",
    headers: {
        Authorization: authorization
    },
    params: params
});

var loginHandler = () =>
    requestify.post(loginURL, getLoginPayload())
        .then(response => {
            var body = response.getBody();
            return body.securityToken;
        });


var changeColor = (color, token) =>
    requestify.request(baseURL + "/device/set", constructBasicPattern(token, {
        idx: idx,
        color: color
    })).then(response => response.getBody());

var turnOn = (on, token) =>
    requestify.request(baseURL + "/device/set", constructBasicPattern(token, {
        idx: idx,
        onoff: on ? 1 : 0
    })).then(response => response.getBody());

/*
    params: intent String One of Good Happy Warn Cheerful Danger
*/
var setIntent = intent =>
    loginHandler().then(token => changeColor(intentMap[intent] ? intentMap[intent] : "FFFFFF", token));

module.exports = {
    login: loginHandler,
    changeColor: changeColor,
    turnOn: turnOn,
    setIntent: setIntent
};
