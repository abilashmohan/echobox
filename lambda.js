/* eslint-disable no-console */
var rechargeController = require("./rechargeController");
var alexaUtils = require("./alexaUtils");

// Route the incoming request based on type (LaunchRequest, IntentRequest,
// etc.) The JSON body of the request is provided in the event parameter.
console.log("Initializing the Lambda...");
exports.handler = function (event, context) {
    try {
        console.log("event.session.application.applicationId=" + event.session.application.applicationId);
        // if (event.session.application.applicationId !== "amzn1.echo-sdk-ams.app.[unique-value-here]") {
        //      context.fail("Invalid Application ID");
        // }

        if (event.session.new)  /* then */ onSessionStarted({requestId: event.request.requestId}, event.session);
        if (event.request.type === "LaunchRequest")  /* then */ onLaunch(event.request, event.session, function (a, b) { context.succeed(alexaUtils.buildResponse(a, b)); });
        else if (event.request.type === "IntentRequest") /* then */ onIntent(event.request, event.session, function (a, b) { context.succeed(alexaUtils.buildResponse(a, b)); });
        else if (event.request.type === "SessionEndedRequest") {
            rechargeController.onSessionEnded(event.request, event.session);
            context.succeed();
        }
    } catch (e) {
        console.log(e);
        context.fail("Exception: " + e);
    }
};

/**
 * Called when the session starts.
 */
var onSessionStarted = function (sessionStartedRequest, session) {
    console.log("onSessionStarted requestId=" + sessionStartedRequest.requestId +
        ", sessionId=" + session.sessionId);
};

/**
 * Called when the user launches the skill without specifying what they want.
 */
var onLaunch = function (launchRequest, session, callback) {
    console.log("onLaunch requestId=" + launchRequest.requestId +
        ", sessionId=" + session.sessionId);

    // Dispatch to your skill's launch.
    rechargeController.getWelcomeResponse(callback);
};

/**
 * Called when the user specifies an intent for this skill.
 */
function onIntent(intentRequest, session, callback) {
    console.log("onIntent requestId=" + intentRequest.requestId +
        ", sessionId=" + session.sessionId);

    var intent = intentRequest.intent,
        intentName = intentRequest.intent.name;

    // Dispatch to your skill's intent handlers
    if ("createRechargeData" === intentName) {
        rechargeController.createRechargeData(intent, session, callback);
    } else if ("getAverageRecharge" === intentName) {
        rechargeController.getAverageRecharge(intent, session, callback);
    } else if ("getRechargeCount" === intentName) {
        rechargeController.getRechargeCount(intent, session, callback);
    } else if ("simpleHi" === intentName) {
        rechargeController.simpleHi(intent, session, callback);
    } else if ("netpl" === intentName) {
        rechargeController.getProfitLoss(intent, session, callback);
    } else if ("AMAZON.HelpIntent" === intentName) {
        rechargeController.getWelcomeResponse(callback);
    } else if ("AMAZON.StopIntent" === intentName || "AMAZON.CancelIntent" === intentName) {
        rechargeController.handleSessionEndRequest(callback);
    } else {
        rechargeController.handleUnknonIntent(intent,session,callback);
    }
}



console.log("Done with Initializing");
