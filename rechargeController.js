/* eslint-disable no-console */

var durationConverters = require("./durationConverters");
var dataCreator = require("./dataCreator");
var alexaUtils = require("./alexaUtils");
var cutils = require("./cutils");
var lightify = require("./lightify");
var dataSet = [];

/**
 * Called when the user ends the session.
 * Is not called when the skill returns shouldEndSession=true.
 */
function onSessionEnded(sessionEndedRequest, session) {
    console.log("onSessionEnded requestId=" + sessionEndedRequest.requestId +
        ", sessionId=" + session.sessionId);
    // Add cleanup logic here
    dataSet = [];
}

function handleUnknonIntent(intent, session, callback) {
    var speechOutput = "I'm unsure on how to respond, can you please rephrase your question?";
    var repromptText = "Sorry, can you rephrase?";
    var shouldEndSession = false;
    callback({}, alexaUtils.buildSpeechletResponse("", speechOutput, repromptText, shouldEndSession));
}

// --------------- Functions that control the skill's behavior -----------------------



function getWelcomeResponse(callback) {
    // If we wanted to initialize the session to have some attributes we could add those here.
    var sessionAttributes = {};
    var cardTitle = "Welcome";
    var speechOutput = "Hi there! How can I be of help?";
    // If the user either does not reply to the welcome message or says something that is not
    // understood, they will be prompted again with this text.
    var repromptText = "You can ask me about the recharge stats.";
    var shouldEndSession = false;

    callback(sessionAttributes,
        alexaUtils.buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
}

function handleSessionEndRequest(callback) {
    var cardTitle = "Session Ended";
    var speechOutput = "Thank you for trying the Store King demo!";
    // Setting this to true ends the session and exits the skill.
    var shouldEndSession = true;

    callback({}, alexaUtils.buildSpeechletResponse(cardTitle, speechOutput, null, shouldEndSession));
}

function simpleHi(intent, sesssion, callback) {
    var speechOutput = "Polo";
    var repromptText = "Marco, Polo";
    lightify.setIntent("Cheerful")
        .catch(err => console.error(err))
        .then(() => callback(
            sesssion.attributes,
            alexaUtils.buildSpeechletResponse("Marco Polo", speechOutput, repromptText, false))
        );
}

var getDuration = (intent, session) => intent.slots.duration && intent.slots.duration.value != "" ? intent.slots.duration :
    session.attributes["duration"] ? session.attributes.duration : { value: "P1D" };
        
function getProfitLoss(intent, session, callback) {
    var attributes = session.attributes ? session.attributes : {};
    var moneyMade = 0;
    var duration = getDuration(intent, session);
    var durationMs = durationConverters.amazonDurationToMs(duration);
    if (attributes.netPL && 
        (!intent.slots.duration || intent.slots.duration.value == duration.value)) {
        moneyMade = attributes.netPL;
    } else {
        moneyMade = cutils.currencyRound(Math.random() * (10000 * (durationMs / (24 * 60 * 60 * 1000))));
    }
    var maxMoney = 10000 * (durationMs / 24 * 60 * 60 * 1000);

    if (moneyMade >= maxMoney / 2) {
        lightify.setIntent("Good");
    } else {
        lightify.setIntent("Warn");
    }
    var speechOutput = "Yes, we are netting in about " + moneyMade + " rupees";
    var repromptText = speechOutput;
    attributes.duration = duration;
    attributes.netPL = moneyMade;

    callback(session.attributes, alexaUtils.buildSpeechletResponse("Net Profit", speechOutput, repromptText,false));
}

function getAverageRecharge(intent, session, callback) {
    var speechOutput = "";
    var repromptText = "";
    //checkCreateDataset();
    console.log(intent);
    // var data = cutils.filterRecharges(dataSet,intent.slots.duration).map(el => el.recharge);
    // var average = data.length > 0 ? data.reduce((prev, curr) => prev + curr, 0) / data.length : 0;
    var duration = getDuration(intent, session);
    var average = cutils.currencyRound(Math.random() * 1000);
    speechOutput = "Our customers were averaging at about  " + cutils.currencyRound(average) + " rupees per recharge. ";
    repromptText = "About " + cutils.currencyRound(average) + " rupees per recharge. ";
    if (average > 500) {
        lightify.setIntent("Good");
        speechOutput += "Thats some serious spending by our customers, great going!";
        repromptText += "Good times!";    
    } else {
        lightify.setIntent("Warn");
    }
    var attributes = session.attributes;
    attributes.duration = duration;
    callback(attributes, alexaUtils.buildSpeechletResponse("Average recharges", speechOutput, repromptText, false));
}
var plurelise = (word, count) => count > 1 ? word + "s" : word;

function getRechargeCount(intent, session, callback) {
    // checkCreateDataset();
    // var data = cutils.filterRecharges(dataSet,intent.slots.duration).length;
    var duration = intent.slots.duration ? intent.slots.duration :
        session.attributes["duration"] ? session.attributes.duration : "P1D";
    var data = Math.round(Math.random() * durationConverters.amazonDurationToMs(duration));
    var speechOutput = null;
    var repromptText = null;
    var cardTitle = "Recharge count";
    if (data === 0) {
        speechOutput = "There were no recharges done!";
        repromptText = "No recharges in that time frame";
    } else {
        speechOutput = "There were " + data + plurelise(" recharge",data) + " done during the given time frame";
        repromptText = "Total "+ plurelise("recharge",data) + ", " + data;
    }
    var attributes = session.attributes;
    attributes.duration = duration;
    callback(attributes, alexaUtils.buildSpeechletResponse(cardTitle, speechOutput, repromptText));
}

function createRechargeData(intent, session, callback) {
    var duration = intent.slots.duration;
    var cardTitle = intent.name;
    var date = Date.now();
    var timeFrame = durationConverters.toSeconds(
        durationConverters.parse(duration && duration.value && duration.value != "" ? duration.value : "P5D"));
    var startDate = date - (timeFrame * 1000);
    console.log("Start Date --> ",startDate);
    dataSet = dataCreator.createTimeRange(startDate, date, 60 * 1000); //Interval is in minutes
    var speechOutput = "I have pulled " +dataSet.length+" events";
    var repromptText = dataSet.length + " events pulled!";
    var sessionAttributes = {};

    callback(sessionAttributes, alexaUtils.buildSpeechletResponse(cardTitle, speechOutput, repromptText, false));
}

var checkCreateDataset = () => {
    var currentTime = Date.now();
    if (!dataSet || dataSet.length <= 0) {
        dataSet = dataCreator.createTimeRange(currentTime - durationConverters.toSeconds(durationConverters.parse("P1W")), currentTime, 60 * 60 * 1000);
    }
};


module.exports = {
    onSessionEnded: onSessionEnded,
    handleUnknonIntent: handleUnknonIntent,
    getWelcomeResponse: getWelcomeResponse,
    handleSessionEndRequest: handleSessionEndRequest,
    simpleHi: simpleHi,
    getAverageRecharge: getAverageRecharge,
    getRechargeCount: getRechargeCount,
    createRechargeData: createRechargeData,
    getProfitLoss:getProfitLoss
};