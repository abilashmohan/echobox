var durationConverters = require("./durationConverters");
var currencyRound = (value) => Math.round(value * 100) / 100;

var filterRecharges = (dataSet,duration) => {
    console.log(duration);
    var dtms = durationConverters.amazonDurationToMs(duration);
    if (dtms) {
        var currentTime = Date.now();
        var timeStartRange = currentTime - dtms;
        console.log("--> currentTime : " + currentTime + ", " + timeStartRange);
        return dataSet.filter(el =>
            el.time >= timeStartRange &&
            el.time < currentTime);
    } else {
        return [];
    }
};

module.exports = {
    currencyRound: currencyRound,
    filterRecharges: filterRecharges
};

